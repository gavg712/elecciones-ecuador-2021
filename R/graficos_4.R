library(tidyverse)
source("R/helpers.R")
#source("R/get_cne_data_prov.R")
load("data/rda/cne_data.rda")

# Grafico 4: Aporte porcentual de Mujeres y Hombres al porcentaje de elección 
#            del candidato (4 candidatos más votados)
map_df(set_names(presid, nm = map_chr(presid, 5)), 
       1, .id = "provincia") %>%
  anti_join(tibble(provincia = "Todas")) %>%
  group_by(provincia) %>%
  mutate(peso_provincia = intVotos/sum(intVotos)) %>%
  ungroup() %>%
  group_by(Candidato = strNomCandidato) %>%
  transmute(Provincia = provincia,
            Mujeres = peso_provincia * intVotosM/sum(peso_provincia),
            Hombres = peso_provincia * intVotosH/sum(peso_provincia),
            Mujeres = Mujeres/sum(Mujeres),
            Hombres = -1 * Hombres/sum(Hombres)) %>%
  ungroup() %>%
  mutate("Diferencia M vs H" = abs(Mujeres - abs(Hombres)) * 100) %>%
  pivot_longer(Mujeres:Hombres, names_to = "Género") %>%
  semi_join(tibble(Candidato = c("ANDRES ARAUZ GALARZA", "GUILLERMO LASSO MENDOZA",
                                 "XAVIER HERVAS", "YAKU PEREZ"))) %>%
  mutate(Provincia = factor(Provincia, levels = nomProvincia)) %>%
  ggplot(aes(Provincia,value, fill = `Género`)) +
  geom_col() +
  geom_point(aes(x = Provincia, y = 0, size = `Diferencia M vs H`)) +
  scale_y_continuous(labels = scales::percent_format(accuracy = 0.01)) +
  facet_wrap(~Candidato, ncol = 8) +
  coord_flip() +
  labs(title = "Porcentaje de Mujeres vs Hombres por provincia",
       subtitle = glue::glue("Fecha de corte: {presid[[1]]$fechaCorte} GMT"),
       x = "", y = '') +
  theme_bw()